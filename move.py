# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date
from decimal import Decimal

from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.transaction import Transaction

_ZERO = Decimal('0.0')
TODAY = date.today()


class Line(metaclass=PoolMeta):
    __name__ = 'account.move.line'
    operation_center = fields.Many2One('company.operation_center',
        'Operation Center')

    @classmethod
    def __setup__(cls):
        super(Line, cls).__setup__()
        cls._check_modify_exclude |= {'operation_center'}


class ReconcileLinesWriteOff(metaclass=PoolMeta):
    __name__ = 'account.move.reconcile_lines.writeoff'

    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', required=True)
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', required=True)


class ReconcileLines(metaclass=PoolMeta):
    __name__ = 'account.move.reconcile_lines'

    def transition_reconcile(self):
        context = Transaction().context
        context['operation_center'] = self.writeoff.operation_center.id if hasattr(self.writeoff, 'operation_center') else None
        account = self.writeoff.analytic_account.id if hasattr(self.writeoff, 'analytic_account') else None
        context['analytic_account'] = account
        with Transaction().set_context(context):
            super(ReconcileLines, self).transition_reconcile()
        return 'end'
