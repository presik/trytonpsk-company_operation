from trytond.pool import PoolMeta
from sql import Table
from trytond.transaction import Transaction
from trytond.model import fields


class Production(metaclass=PoolMeta):
    __name__ = 'production'
    operation_center = fields.Many2One('company.operation_center', 'Operation Center')

    @classmethod
    def get_consumption_line(cls, record, product, account, debit, credit, company):
        line = super(Production, cls).get_consumption_line(record, product, account, debit, credit, company)
        line['operation_center'] = record.operation_center or record.warehouse.operation_center
        return line

    @classmethod
    def get_line_expense(cls, production, input, company):
        line, amount = super(Production, cls).get_line_expense(production, input, company)
        line['operation_center'] = production.operation_center or production.warehouse.operation_center
        return line, amount

    @classmethod
    def get_line_cost(cls, record, cost, company):
        line = super(Production, cls).get_line_cost(record, cost, company)
        line['operation_center'] = record.operation_center or record.warehouse.operation_center
        return line


class ProcessProductionAsync(metaclass=PoolMeta):
    __name__ = 'production.process_production_async'

    def create_move(self, lines, journal, period_id):
        move = super(ProcessProductionAsync, self).create_move(lines, journal, period_id)
        cursor = Transaction().connection.cursor()
        move_line = Table('account_move_line')
        cursor.execute(*move_line.update(
                    columns=[move_line.operation_center],
                    values=[self.start.shop.operation_center.id],
                    where=move_line.move.in_([move.id]))
                )
        return move
