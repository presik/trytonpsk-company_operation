from collections import defaultdict
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.model import fields
from trytond.tools import reduce_ids, grouped_slice, cursor_dict


class Line(metaclass=PoolMeta):
    'Analytic Line'
    __name__ = 'analytic_account.line'

    operation_center = fields.Function(fields.Many2One('company.operation_center', 'Operation Center'),
        'get_values')

    party = fields.Function(fields.Many2One('party.party', 'Party'),
        'get_values')

    description = fields.Function(fields.Char('Description'),
        'get_values')
    origin = fields.Function(fields.Reference("Move Origin", selection='get_move_origin'),
        'get_values')

    @classmethod
    def get_move_origin(cls):
        Move = Pool().get('account.move')
        return Move.get_origin()

    @classmethod
    def get_values(cls, records, names):
        Moveline = Pool().get('account.move.line')
        moveline = Moveline.__table__()
        line = cls.__table__()
        cursor = Transaction().connection.cursor()
        # initialize result and columns
        result = defaultdict(defaultdict)
        columns = [
            line.id.as_('id'),
            moveline.party.as_('party'),
            moveline.operation_center.as_('operation_center'),
            moveline.description.as_('description'),
            ]
        ids = []
        ids_append = ids.append
        for row in records:
            move_line = row.move_line
            ids_append(move_line)
            result['origin'][row.id] = str(move_line.move_origin) if move_line.move_origin else None
        for sub_ids in grouped_slice(ids):
            cursor.execute(*line.join(moveline,
                condition=line.move_line == moveline.id
                ).select(*columns,
                where=reduce_ids(moveline.id, sub_ids),
                ),)

            for row in cursor_dict(cursor):
                id = row['id']
                result['party'][id] = row['party']
                result['operation_center'][id] = row['operation_center']
                result['description'][id] = row['description']
        for key in list(result.keys()):
            if key not in names:
                del result[key]
        return result
