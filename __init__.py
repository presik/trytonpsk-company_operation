# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool

from . import (
    account,
    account_reporting,
    analytic_account,
    asset,
    invoice,
    move,
    operation_center,
    production,
    purchase,
    sale,
    staff,
    stock,
    user,
    voucher,
)

# from . import sale_contract


def register():
    Pool.register(
        operation_center.OperationCenter,
        operation_center.OperationCenterUser,
        invoice.Invoice,
        sale.Sale,
        invoice.PaymentsByOCStart,
        invoice.InvoicesStart,
        invoice.InvoiceLine,
        # invoice.PayInvoiceAsk,
        move.ReconcileLinesWriteOff,
        purchase.Purchase,
        purchase.PurchaseLine,
        # purchase.PurchaseRequisition,
        # purchase.PurchaseRequisitionLine,
        # purchase.PurchaseRequest,
        # sale_contract.ContractProductLine,
        # sale_contract.SaleContractLine,
        # sale.SaleLine,
        voucher.Voucher,
        voucher.VoucherLine,
        voucher.NoteLine,
        voucher.Note,
        move.Line,
        stock.Location,
        stock.ShipmentIn,
        stock.ShipmentInternal,
        stock.Invetory,
        # purchase.PurchaseUpdateStart,
        account.AuxiliaryByOCStart,
        account_reporting.Context,
        account_reporting.OperationCenter,
        account_reporting.OperationCenterTimeseries,
        analytic_account.Line,
        voucher.AddZeroAdjustmentStart,
        staff.Employee,
        staff.Payroll,
        staff.Liquidation,
        staff.PayrollLine,
        asset.Asset,
        user.OpCenterResUser,
        user.User,
        module='company_operation', type_='model')
    Pool.register(
        production.Production,
        module='company_operation', type_='model',
        depends=['production_accounting'])
    Pool.register(
        production.ProcessProductionAsync,
        module='company_operation', type_='wizard',
        depends=['production_accounting'])
    Pool.register(
        # purchase.PurchaseUpdate,
        purchase.CreatePurchase,
        invoice.PaymentsByOC,
        invoice.Invoices,
        voucher.AddZeroAdjustment,
        account.AuxiliaryByOC,
        move.ReconcileLines,
        staff.PayrollGroup,
        module='company_operation', type_='wizard')
    Pool.register(
        invoice.PaymentsByOCReport,
        account.AuxiliaryByOCReport,
        invoice.InvoicesReport,
        account_reporting.OperationCenterReport,
        account_reporting.OperationCenterDetailedReport,
        module='company_operation', type_='report')
