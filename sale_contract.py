# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class ContractProductLine(metaclass=PoolMeta):
    "Contract Product Line"
    __name__ = 'sale.contract.product_line'

    operation_center = fields.Many2One('company.operation_center',
        'Operation Center', states={
            'readonly': ~Eval('contract_state').in_(['draft', None]),
        },
        depends=['contract_state'], required=True)


class SaleContractLine(metaclass=PoolMeta):
    "Sale Contract Line"
    __name__ = 'sale.contract.line'

    def get_sale_line(self, contract, line):
        sale_line = super(SaleContractLine, self).get_sale_line(contract, line)
        if line.operation_center:
            sale_line['operation_center'] = line.operation_center
        return sale_line
